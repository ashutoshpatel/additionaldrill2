// import the data.js
const got = require('./data.js');


// 1st problem : counts the total number of people

// declare the countAllPeople function
function countAllPeople(obj){

    if(Object.keys(obj).length === 0){
        // means the object is empty
        return null;
    }

    const totalNumberOfPeople = Object.values(obj)[0].reduce( (count, currentObj) => {
        // calculate the total people in people key array
        count = count + currentObj.people.length;
        // return count to next step
        return count;
    }, 0);

    // return the answer
    return totalNumberOfPeople;
}

// call the countAllPeople function
const totalNumberOfPeople = countAllPeople(got);
if(totalNumberOfPeople !== null){
    // print the output
    console.log(totalNumberOfPeople);
}

// <------------------------------------------------------------------------------------->
// 2nd problem : counts the total number of people in different houses

// declare the peopleByHouses function
function peopleByHouses(obj){

    if(Object.keys(obj).length === 0){
        // means the object is empty
        return null;
    }

    const totalNumberOfPeopleInHouse = Object.values(obj)[0].reduce( (acc, currentObj) => {
        // get the current house name
        const houseName = currentObj.name;
        // calculate the how many people present in that house
        // and add in acc object
        acc[houseName] = currentObj.people.length;
        // return the acc object to next step
        return acc;
    }, {});

    // return the answer
    return totalNumberOfPeopleInHouse;
}

// call the peopleByHouses function
const totalNumberOfPeopleInHouse = peopleByHouses(got);
if(totalNumberOfPeopleInHouse !== null){
    // print the output
    console.log(totalNumberOfPeopleInHouse);
}

// <------------------------------------------------------------------------------------->
// 3rd problem : return array of names of all the people

// declare the everyone function
function everyone(obj){

    if(Object.keys(obj).length === 0){
        // means the object is empty
        return null;
    }

    const nameOfAllThePeople = Object.values(obj)[0].reduce( (acc, currentObj) => {
        // get the people and store in peopleArray
        const peopleArray = currentObj.people;

        // use forEach to iterate on peopleArray and get name of people
        peopleArray.forEach( (currentValue) => {
            // push the people name in acc array
            acc.push(currentValue.name);
        })

        // return the acc array to next step
        return acc;
    }, []);

    // return the answer
    return nameOfAllThePeople;
}

// call the everyone function
const nameOfAllThePeople = everyone(got);
if(nameOfAllThePeople !== null){
    // print the output
    console.log(nameOfAllThePeople);
}

// <------------------------------------------------------------------------------------->
// 4th problem : returns a array of names of all the people whose name includes `s` or `S`

// declare the nameWithS function
function nameWithS(obj){

    if(Object.keys(obj).length === 0){
        // means the object is empty
        return null;
    }

    const nameOfAllThePeopleWithSCharacter = Object.values(obj)[0].reduce( (acc, currentObj) => {
        // get the people and store in peopleArray
        const peopleArray = currentObj.people;

        // use forEach to iterate on peopleArray and get name of people
        peopleArray.forEach( (currentValue) => {
            // get the current people name
            const currentPeopleName = currentValue.name;

            // check the people name have character 's' or 'S' in name
            if(currentPeopleName.includes('s') || currentPeopleName.includes('S')){
                // if yes then push the people name in acc array
                acc.push(currentPeopleName);
            }
        });

        // return the acc array to next step
        return acc;
    }, []);

    // return the answer
    return nameOfAllThePeopleWithSCharacter;
}

// call the nameWithS function
const nameOfAllThePeopleWithSCharacter = nameWithS(got);
if(nameOfAllThePeopleWithSCharacter !== null){
    // print the output
    console.log(nameOfAllThePeopleWithSCharacter);
}


// <------------------------------------------------------------------------------------->
// 5th problem : returns a array of names of all the people whose name includes `a` or `A`

// declare the nameWithA function
function nameWithA(obj){

    if(Object.keys(obj).length === 0){
        // means the object is empty
        return null;
    }

    const nameOfAllThePeopleWithACharacter = Object.values(obj)[0].reduce( (acc, currentObj) => {
        // get the people and store in peopleArray
        const peopleArray = currentObj.people;

        // use forEach to iterate on peopleArray and get name of people
        peopleArray.forEach( (currentValue) => {
            // get the current people name
            const currentPeopleName = currentValue.name;

            // check the people name have character 'a' or 'A' in name
            if(currentPeopleName.includes('a') || currentPeopleName.includes('A')){
                // if yes then push the people name in acc array
                acc.push(currentPeopleName);
            }
        });

        // return the acc array to next step
        return acc;
    }, []);

    // return the answer
    return nameOfAllThePeopleWithACharacter;
}

// call the nameWithA function
const nameOfAllThePeopleWithACharacter = nameWithA(got);
if(nameOfAllThePeopleWithACharacter !== null){
    // print the output
    console.log(nameOfAllThePeopleWithACharacter);
}

// <------------------------------------------------------------------------------------->
// 6th problem : returns a array of names of all the people whoes surname is starting with `S`(capital s)

// declare the surnameWithS function
function surnameWithS(obj){

    if(Object.keys(obj).length === 0){
        // means the object is empty
        return null;
    }

    const surnameOfAllPeopleWithCharacterS = Object.values(obj)[0].reduce( (acc, currentObj) => {
        // get the people and store in peopleArray
        const peopleArray = currentObj.people;

        // use forEach to iterate on peopleArray and get name of people
        peopleArray.forEach( (currentValue) => {
            // get the current people name
            const currentPeopleName = currentValue.name;
            // get the surname of current people name
            const surname = currentPeopleName.split(" ");

            // check the people surname is starting with capital s
            if(surname[1].startsWith('S')){
                // if yes then push the people name in acc array
                acc.push(currentPeopleName);
            }
        });

        // return the acc array to next step
        return acc;
    }, []);

    // return the answer
    return surnameOfAllPeopleWithCharacterS;
}

// call the surnameWithS function
const surnameOfAllPeopleWithCharacterS = surnameWithS(got);
if(surnameOfAllPeopleWithCharacterS !== null){
    // print the output
    console.log(surnameOfAllPeopleWithCharacterS);
}

// <------------------------------------------------------------------------------------->
// 7th problem : returns a array of names of all the people whoes surname is starting with `A`(capital a)

// declare the surnameWithA function
function surnameWithA(obj){

    if(Object.keys(obj).length === 0){
        // means the object is empty
        return null;
    }

    const surnameOfAllPeopleWithCharacterA = Object.values(obj)[0].reduce( (acc, currentObj) => {
        // get the people and store in peopleArray
        const peopleArray = currentObj.people;

        // use forEach to iterate on peopleArray and get name of people
        peopleArray.forEach( (currentValue) => {
            // get the current people name
            const currentPeopleName = currentValue.name;
            // get the surname of current people name
            const surname = currentPeopleName.split(" ");

            // check the people surname is starting with capital a
            if(surname[1].startsWith('A')){
                // if yes then push the people name in acc array
                acc.push(currentPeopleName);
            }
        });

        // return the acc array to next step
        return acc;
    }, []);

    // return the answer
    return surnameOfAllPeopleWithCharacterA;
}

// call the surnameWithA function
const surnameOfAllPeopleWithCharacterA = surnameWithA(got);
if(surnameOfAllPeopleWithCharacterA !== null){
    // print the output
    console.log(surnameOfAllPeopleWithCharacterA);
}

// <------------------------------------------------------------------------------------->
// 8th problem : returns an object with the key of the name of house and value will be all the people in the house in an array

// declare the peopleNameOfAllHouses function
function peopleNameOfAllHouses(obj){

    if(Object.keys(obj).length === 0){
        // means the object is empty
        return null;
    }

    const nameOfHouseWithPeopleName = Object.values(obj)[0].reduce( (acc, currentObj) => {
        // get the house name
        const currentHouseName = currentObj.name;
        // get the people and store in peopleArray
        const peopleArray = currentObj.people;

        // use forEach to iterate on peopleArray and get name of people
        peopleArray.forEach( (currentValue) => {
            // get the current people name
            const currentPeopleName = currentValue.name;

            // check the current house name is present in acc object 
            if(acc[currentHouseName]){
                // if yes then push the people name in acc object
                acc[currentHouseName].push(currentPeopleName);
            }
            else{
                // if the currentHouseName is not present in acc object
                // add with the current people name
                acc[currentHouseName] = [currentPeopleName];
            }
        });

        // return the acc object to next step
        return acc;
    }, {});

    // return the answer
    return nameOfHouseWithPeopleName;
}

// call the peopleNameOfAllHouses function
const nameOfHouseWithPeopleName = peopleNameOfAllHouses(got);
if(nameOfHouseWithPeopleName !== null){
    // print the output
    console.log(nameOfHouseWithPeopleName);
}